package prjava00.yudesa;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Prjava00YudesA {
    public static void main(String[] args) {
        System.out.println("Projecte de Andrea Yudes");
        System.out.println("Versió 0.2 del projecte prjava00-YudesA");
        
        try {
            InetAddress addr = InetAddress.getLocalHost();
            byte[] ipAddr = addr.getAddress();
            String hostname = addr.getHostName();
            
            System.out.println("hostname = " + hostname);
            System.out.println("Nom de l'usuari: " + System.getProperty("user.name"));
            System.out.println("Carpeta personal: " + System.getProperty("user.home"));
            System.out.println("Sistema operatiu: " + System.getProperty("os.name"));
            System.out.println("Versió OS: " + System.getProperty("os.version"));
        } catch(UnknownHostException e) {
            e.printStackTrace();
        }
    }
    
}
